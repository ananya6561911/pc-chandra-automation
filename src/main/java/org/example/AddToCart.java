package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AddToCart {
    private WebDriver driver;

    @BeforeMethod
    public void setUp() {

//Setting system properties of ChromeDriver

        System.setProperty("web-driver.chrome.driver", "C://Users//SDS//Downloads//chromedriver-win64//chromedriver-win64//chromedriver.exe/");
        System.setProperty("web-driver.http.factory", "jdk-http-client");

        driver = new ChromeDriver();
        driver.manage().window().maximize();

    }
    @Test
    public void testNewsletterFormSubmission() throws InterruptedException {

//Automate to Navigate Homepage

        driver.get("https://pcchandraindia.com/");
        Thread.sleep(1000);

//Maximize current window

        driver.manage().window().maximize();
        Thread.sleep(7000);
// Automate Accept Cookie

        driver.findElement(By.xpath("//a[@class='cc-btn cc-allow']")).click();
        Thread.sleep(7000);

// Find products in search bar

        WebElement searchBtn = driver.findElement(By.xpath("//li[@class='li-search for-desktop']//a"));
        searchBtn.click();

        WebElement searchBox = driver.findElement(By.id("search_keyword"));

        searchBox.	sendKeys("14KTDIJFPA3003");
        searchBox.sendKeys(Keys.ENTER);

        Thread.sleep(2000);

// Add to wishlist

        WebElement wishlist = driver.findElement(By.xpath("//div[@class='product_top']//div[@class='wishlist']"));
        Thread.sleep(3000);

        wishlist.	click();
        Thread.sleep(2000);

// Go to Wishlist page

        driver.findElement(By.xpath("/html/body/header/div[1]/div/div/div[2]/ul/li[8]/a")).click();
        Thread.sleep(1000);


// Locating the element

        WebElement e = driver.findElement(By.xpath("/html/body/main/section[2]/div/div/div[2]/div[2]/div[2]/div/div/a[2]"));
        driver.findElement(By.className("btn bg--pinkMid c--white hvr:bg--darkgray btn-red add-to-cart")).click();
//move to cart
        driver.findElement(By.xpath("/html/body/header/div[1]/div/div/div[2]/ul/li[6]/a/span")).click();
        driver.findElement(By.xpath("/html/body/div[4]/div/div[4]/a")).click();
// proceed to checkout
        driver.findElement(By.xpath("/html/body/main/section[2]/div/div/div[3]/div[2]/div/div/a[2]")).click();

        driver.findElement(By.xpath("/html/body/header/div[1]/div/div/div[2]/ul/li[4]/a")).click();
        System.out.println("Navigate to Account Login");
        driver.findElement(By.id("login_email"));
        WebElement username=driver.findElement(By.id("login_email"));
        WebElement password=driver.findElement(By.id("password"));

        System.out.println("entering the email");
        username.sendKeys("ananya@sundewsolutions.com");
        Thread.sleep(5000);

        System.out.println("entering the Password");
        password.sendKeys("XIje$(84");
        Thread.sleep(5000);

        System.out.println("Click On submit Button");
        driver.findElement(By.id("login_btn")).click();


        driver.quit();

    }

}
